#import "FlutterRollbarPlugin.h"
#if __has_include(<flutter_rollbar/flutter_rollbar-Swift.h>)
#import <flutter_rollbar/flutter_rollbar-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutter_rollbar-Swift.h"
#endif

@implementation FlutterRollbarPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterRollbarPlugin registerWithRegistrar:registrar];
}
@end
